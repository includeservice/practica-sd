package com.example.demo.Repositories;

import com.example.demo.Classes.Coin;
import com.example.demo.Classes.Provider;
import com.example.demo.Classes.Type;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;

/**
 * Interfaz de queries a la tabla de Coins de la base de datos
 */
public interface CoinRepository extends JpaRepository<Coin, Long> {
    //Metodos SELECT en funcion de atributos
    Coin findById(long id);
    List<Coin> findByYear(int year);
    List<Coin> findByCoinageCity(String city);
    List<Coin> findByConservation(String conservation);
    List<Coin> findByProviderName(String provider);
    List<Coin> findByAcquisitionDate(Date date);
    List<Coin> findByTypeValue(int type);

    //Metodos SELECT (ORDER BY) en funcion de atributos
    List<Coin> OrderByYear();
    List<Coin> OrderByType();
    List<Coin> OrderByCoinageCity();
    List<Coin> OrderByProvider();
    List<Coin> OrderByAcquisitionDate();
    List<Coin> OrderByConservation();

    //Redefinicion del metodo update
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Coin c set c.year = ?2, c.coinageCity = ?3, c.acquisitionDate = ?4, c.conservation = ?5, c.type = ?6, c.provider = ?7 where c.id = ?1")
    void updateCoin(long id, int year, String coinageCity, Date acquisitionDate, String conservation, Type type, Provider provider);
}
