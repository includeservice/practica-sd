package com.example.demo.Repositories;

import com.example.demo.Classes.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Interfaz e queries a la tabla de Types de la base de datos
 */
public interface TypeRepository extends JpaRepository<Type, Long> {
    //Metodos SELECT en funcion de atributos
    Type findById(long id);
    List<Type> findByValue(int value);
    List<Type> findByMonetaryUnit(String monetaryUnit);
    List<Type> findByDiameter(double diameter);
    List<Type> findByWeight(double weight);
    List<Type> findByMetals(String metals);
    //Metodos SELECT (ORDER BY) en funcion de atributos
    List<Type> OrderByValue();
    List<Type> OrderByMonetaryUnit();
    List<Type> OrderByDiameter();
    List<Type> OrderByWeight();
    List<Type> OrderByMetals();

    //Redefinicion del metodo update
    @Modifying
    @Query("UPDATE Type t set t.value = ?2, t.monetaryUnit = ?3, t.diameter = ?4, t.weight = ?5, t.metals = ?6, t.description = ?7 where t.id = ?1")
    void updateType(long id, int value, String monetaryUnit, double diameter, double weight, String metals, String description);
}
