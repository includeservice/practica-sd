package com.example.demo.Repositories;

import com.example.demo.Classes.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Interfaz de queries a la tabla de Providers de la base de datos
 */
public interface ProviderRepository extends JpaRepository<Provider, String> {
    //Metodos SELECT en funcion de atributos
    List<Provider> findByName(String name);
    List<Provider> findByCif(String cif);
    List<Provider> findByAddress(String address);
    List<Provider> findByEmail(String email);
    List<Provider> findByPhone(String phone);
    //Metodos SELECT (ORDER BY) en funcion de atributos
    List<Provider> OrderByName();
    List<Provider> OrderByCif();
    List<Provider> OrderByAddress();
    List<Provider> OrderByEmail();
    List<Provider> OrderByPhone();

    //Redefinicion del metodo update
    @Modifying
    @Query("UPDATE Provider p set p.name = ?2, p.address = ?3, p.email = ?4, p.phone = ?5 where p.cif = ?1")
    void updateProvider(String cif, String name, String address, String email, String phone);
}
