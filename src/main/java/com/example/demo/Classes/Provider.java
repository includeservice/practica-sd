package com.example.demo.Classes;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Table Provider (java bean)
 */
@SuppressWarnings("unused")
@Entity
public class Provider {

    public enum SortProvider{CIF, NAME, ADDRESS, EMAIL, PHONE}

    //Primary key:
    @Id
    private String cif;
    //Atributes:
    private String name;
    private String address;
    private String email;
    private String phone;
    //Relation oneToMany:
    @OneToMany(mappedBy = "provider")
    private Collection<Coin> coins;

    //CONSTRUCTORS
    public Provider() {
    }

    public Provider(String cif, String name, String address, String email, String phone) {
        this.cif = cif;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.coins = new ArrayList<>();
    }

    //GETTERS & SETTERS
    public String getCif() {
        return cif;
    }

    public void setCif(String dni) {
        this.cif = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Collection<Coin> getCoins() {
        return coins;
    }

    public void setCoins(Collection<Coin> coins) {
        this.coins = coins;
    }

    //EQUALS
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Provider)) return false;
        Provider provider = (Provider) o;
        if(cif != null)
            return cif.equals(provider.cif) || name.equals(provider.name);
        else
            return name.equals(provider.name);
    }

    //HASHCODE
    @Override
    public int hashCode() {
        return Objects.hash(cif);
    }
}
