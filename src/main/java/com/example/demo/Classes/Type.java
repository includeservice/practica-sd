package com.example.demo.Classes;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * Table Type (Java bean)
 */
@SuppressWarnings("unused")
@Entity
public class Type {

    public enum SortType{VALUE, MONETARY_UNIT, DIAMETER, WEIGHT, METALS}

    //Primary key:
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    //Atributes:
    private int value;
    private String monetaryUnit;
    private double diameter;
    private double weight;
    private String metals;
    private String description;
    //Relation oneToMany:
    @OneToMany(mappedBy = "type")
    private Collection<Coin> coins;

    //CONSTRUCTORS:
    public Type() {
    }

    public Type(int value, String monetaryUnit, double diameter, double weight, String metals, String description) {
        this.value = value;
        this.monetaryUnit = monetaryUnit;
        this.diameter = diameter;
        this.weight = weight;
        this.metals = metals;
        this.description = description;
        this.coins = new ArrayList<>();
    }

    // GETTERS & SETTERS
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public long getId() {
        return id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMonetaryUnit() {
        return monetaryUnit;
    }

    public void setMonetaryUnit(String monetaryUnit) {
        this.monetaryUnit = monetaryUnit;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public String getMetals() {
        return metals;
    }

    public void setMetals(String metals) {
        this.metals = metals;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Collection<Coin> getCoins() {
        return coins;
    }

    public void setCoins(Collection<Coin> coins) {
        this.coins = coins;
    }

    //EQUALS
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Type)) return false;
        Type type = (Type) o;
        return id == type.id;
    }

    //HASHCODE
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
