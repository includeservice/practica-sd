package com.example.demo.Classes;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.*;

/**
 * Table coin (Java bean)
 */
@SuppressWarnings("unused")
@Entity
public class Coin {

    public enum SortCoin {YEAR, COINAGE_CITY, ACQUISITION_DATE, CONSERVATION, TYPE, PROVIDER}

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    //Primary key:
    private long id;
    //Atributes
    private int year;
    private String coinageCity;
    private Date acquisitionDate;
    private String conservation;
    //Foreing key:
    @ManyToOne
    @JoinColumn(name="id_type")
    private Type type;
    //Foreing key:
    @ManyToOne
    @JoinColumn(name="id_provider")
    private Provider provider;

    //CONSTRUCTORS:
    public Coin(){
    }

    public Coin(Type type, int year, String coinageCity, Date acquisitionDate, String conservation, Provider provider) {
        this.type = type;
        this.year = year;
        this.coinageCity = coinageCity;
        this.acquisitionDate = acquisitionDate;
        this.conservation = conservation;
        this.provider = provider;
    }

    //GETTERS & SETTERS
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCoinageCity() {
        return coinageCity;
    }

    public void setCoinageCity(String coinageCity) {
        this.coinageCity = coinageCity;
    }

    public Date getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(Date acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public String getConservation() {
        return conservation;
    }

    public void setConservation(String conservation) {
        this.conservation = conservation;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    //EQUALS
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coin)) return false;
        Coin coin = (Coin) o;
        return id == coin.id;
    }

    //HASHCODE
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
