package com.example.demo.Controllers;

import com.example.demo.Classes.Type;
import com.example.demo.Repositories.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Models (Types) Controller
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Controller
public class ModelsController {

    //REPOSITORY:
    @Autowired
    TypeRepository typeRepository;

    //Types shown in page
    private Set<Type> shownTypes = new HashSet<>();

    /**
     * Show the Types contained in the database
     */
    @RequestMapping("/models")
    public String show(Model model) {
        DatabaseController.selection = DatabaseController.MODELS;
        shownTypes.clear();
        shownTypes.addAll(typeRepository.findAll());
        model.addAttribute(DatabaseController.MODELS, shownTypes);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.MODELS);
        return "explorer_database";
    }

    /**
     * Show a form in order to insert a new Type in the database
     */
    @RequestMapping("/new_model")
    public String newModel(Model model){
        model.addAttribute("descripcion", "");
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.MODELS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.CREATE);
        return "input_form_database";
    }

    /**
     * Show a form in order to edit a Type in the database
     */
    @RequestMapping("/edit_model={id}")
    public String editModel(@PathVariable(value = "id")long id, Model model){
        model.addAttribute("type", typeRepository.findById(id));
        model.addAttribute("descripcion", typeRepository.findById(id).getDescription());
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.MODELS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.EDIT);
        return "input_form_database";
    }

    @Transactional
    @RequestMapping("/update_model={id}")
    public RedirectView newModel(@PathVariable(value = "id")long id, Type type){
        typeRepository.updateType(id, type.getValue(), type.getMonetaryUnit(), type.getDiameter(),
                type.getWeight(), type.getMetals(), type.getDescription());
        return new RedirectView("/models");
    }

    @RequestMapping("/delete_model={id}")
    public RedirectView deleteCoin(@PathVariable(value = "id")long id){
        if(typeRepository.existsById(id))
            typeRepository.deleteById(id);
        return new RedirectView("/models");
    }

    /**
     * Inserts a new Type in the database
     */
    @RequestMapping("/save_model")
    public RedirectView newModel(Type type){
        typeRepository.save(type);
        return new RedirectView("/models");
    }

    /**
     * Select types from the database depending on the search variable
     */
    @RequestMapping("/search_type")
    public String searchType(@RequestParam(value = "search") String search, Model model){
        shownTypes.clear();

        try {
            shownTypes.addAll(typeRepository.findByValue(Integer.parseInt(search)));
        }catch (Exception ignored){}
        try {
            shownTypes.addAll(typeRepository.findByDiameter(Double.parseDouble(search)));
        } catch (Exception ignored){}
        try {
            shownTypes.addAll(typeRepository.findByWeight(Double.parseDouble(search)));
        } catch (Exception ignored){}

        shownTypes.addAll(typeRepository.findByMetals(search));
        shownTypes.addAll(typeRepository.findByMonetaryUnit(search));

        model.addAttribute(DatabaseController.MODELS, shownTypes);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.MODELS);

        return "explorer_database";
    }

    /**
     * Sort the shown Types depending on the sort variable
     */
    @RequestMapping("/sort_type={sort}")
    public String sortModel(@PathVariable(value = "sort") Type.SortType sort, Model model){
        List<Type> allModel = new ArrayList<>();
        if(shownTypes.isEmpty())
            shownTypes.addAll(typeRepository.findAll());

        switch (sort){
            case VALUE:
                allModel.addAll(typeRepository.OrderByValue());
                break;
            case DIAMETER:
                allModel.addAll(typeRepository.OrderByDiameter());
                break;
            case METALS:
                allModel.addAll(typeRepository.OrderByMetals());
                break;
            case MONETARY_UNIT:
                allModel.addAll(typeRepository.OrderByMonetaryUnit());
                break;
            case WEIGHT:
                allModel.addAll(typeRepository.OrderByWeight());
                break;
            default:
                allModel.addAll(shownTypes);
        }

        allModel.retainAll(shownTypes);

        model.addAttribute(DatabaseController.MODELS, allModel);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.MODELS);

        return "explorer_database";
    }
}