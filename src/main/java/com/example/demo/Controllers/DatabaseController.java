package com.example.demo.Controllers;

import com.example.demo.Classes.Coin;
import com.example.demo.Classes.Provider;
import com.example.demo.Classes.Type;
import com.example.demo.Repositories.CoinRepository;
import com.example.demo.Repositories.ProviderRepository;
import com.example.demo.Repositories.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for the database
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Controller
public class DatabaseController {

    //REPOSITORIES:
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private CoinRepository coinRepository;
    @Autowired
    private ProviderRepository providerRepository;

    private List<Coin> coins;
    private List<Provider> providers;
    private List<Type> types;

    static final String SELECTION = "selection";
    static final String COINS = "coins";
    static final String PROVIDERS = "providers";
    static final String MODELS = "types";
    static final String MAIN = "main";

    static final String MODE = "mode";
    static final String EDIT = "edit";
    static final String CREATE = "create";

    static String selection;

    /**
     * Shows the main page
     */
    @RequestMapping("/")
    public String main(Model model){
        selection = MAIN;

        model.addAttribute(SELECTION, selection);
        model.addAttribute(COINS, coins);
        model.addAttribute(MODELS, types);
        model.addAttribute(PROVIDERS, providers);

        return "explorer_database";
    }


    /**
     * Insert values in the database
     */
    @PostConstruct
    public void initialDBValues(){
        newProviders();
        newTypes();
        newCoins();

        //Save providers into db
        for (Provider p: providers){
            providerRepository.save(p);
        }
        //Save types into db
        for (Type t: types){
            typeRepository.save(t);
        }
        //Save coins into db
        for (Coin c: coins){
            coinRepository.save(c);
        }
    }

    /**
     * Creates new Provider objects
     */
    private void newProviders(){
        providers = new ArrayList<>();
        providers.add(new Provider("B0000000A", "Calderilla","C/ de la Moneda, 1","tengodinero@gmail.com","6006000600"));
        providers.add(new Provider("B0000001A", "Ed Sheeran","C/ de la Moneda, 2","ed.sheeran@gmail.com","6006000601"));
        providers.add(new Provider("B0000002A", "Adam Levine","C/ de la Moneda, 3","adam.levine@gmail.com","6006000602"));
        providers.add(new Provider("B0000003A", "Tylor Swift","C/ de la Moneda, 4","tylor.swift@gmail.com","6006000603"));
        providers.add(new Provider("B0000004A", "Miley Cyrus","C/ de la Moneda, 5","miley.cyrus@gmail.com","6006000604"));
        providers.add(new Provider("06632454J", "José Miguel Zamora Batista","C/Donoso Cortes, 20","jm.zamora.2016@alumnos.urjc.es","678656754"));
        providers.add(new Provider("50620153M", "David Correas Oliver","C/ San Guiño, 4","d.correas.2016@alumnos.urjc.es","634009182"));
        providers.add(new Provider("50355253L", "Alejandro Quesada Mendo","C/ Almería, 9","a.quesada.2016@alumnos.urjc.es","622139887"));
    }

    /**
     * Creates new Type objects
     */
    private void newTypes(){
        types =  new ArrayList<>();
        //doblón español de oro
        types.add(new Type(1, "doblón", 2.0, 6.77, "Oro",
                "El doblón fue una moneda de oro española que equivalía a dos escudos o 32 reales"));
        // 100 pesetas (aka "20 duros")
        types.add(new Type (100, "pesetas", 1.0, 1.0, "Latón"+" Niquel",
                "100 Pesetas. Los míticos ``20 duros´´ están representados por esta dorada moneda"));
        //2€
        types.add(new Type(2, "Euro", 2.575, 1.0, "Cobre"+" Niquel"+" Latón",
                "2 euros de toda la vida"));
        //1€
        types.add(new Type(1, "Euro", 2.325, 1.0,"Cobre"+" Niquel"+" Latón",
                "1 euro de toda la vida"));
        //50cents
        types.add(new Type(50, "Centavos EEUU", 2.15, 1.0, "Cobre"+" Niquel",
                "50cents? Es un rapero muy famoso"));
        //500 euros monopoli
        types.add(new Type(500, "Euros monopoli", 6.0, 0.5, "Aluminio",
                "Es un billete rosa para comprar el paseo del prado"));
        //Peso cubano
        types.add(new Type(1, "Peso Cubano", 1.2, 1.0, "Plomo",
                "No te puto pilles"));
        //Lira italiana
        types.add(new Type(1, "Lira Italiana", 3.1, 2.0, "Aluminio",
                "De las que se tiraban antes a la Fontana di Trevi"));

    }

    /**
     * Create new Coin objects
     */
    private void newCoins(){
        coins =  new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        //New Coins
        try {
            Coin c0 = new Coin(types.get(0), 1634, "Valladolid",new Date(sdf.parse("2000/01/01").getTime()),
                    "Muy bien conservada", providers.get(0));
            Coin c1 = new Coin(types.get(1), 1934, "Villaconejos",new Date(sdf.parse("2018/03/01").getTime()),
                    "mMuy nuevecita", providers.get(1));
            Coin c2 = new Coin(types.get(2), 1934, "Huesca",new Date(sdf.parse("2015/09/12").getTime()),
                    "Esta bien, pero ya no brilla como antes", providers.get(2));
            Coin c3 = new Coin(types.get(3), 1934, "Santiago de Compostela",new Date(sdf.parse("1998/06/18").getTime()),
                    "Da gusto mirarla", providers.get(3));
            Coin c4 = new Coin(types.get(4), 1934, "Nueva York",new Date(sdf.parse("2001/05/04").getTime()),
                    "Mejorable", providers.get(4));
            Coin c5 = new Coin(types.get(3), 1969, "Luna",new Date(sdf.parse("2019/04/02").getTime()),
                    "Muy bien", providers.get(0));
            Coin c6 = new Coin(types.get(5), 2019, "Zaragozaaaaa",new Date(sdf.parse("2019/04/03").getTime()),
                    "Muy bien", providers.get(5));
            Coin c7 = new Coin(types.get(6), 2019, "La Habana",new Date(sdf.parse("2019/04/04").getTime()),
                    "Muy bien", providers.get(6));
            Coin c8 = new Coin(types.get(7), 2019, "Roma",new Date(sdf.parse("2019/04/05").getTime()),
                    "Muy bien", providers.get(7));



            //Add new coins to the arrayList
            coins.add(c0);
            coins.add(c1);
            coins.add(c2);
            coins.add(c3);
            coins.add(c4);
            coins.add(c5);
            coins.add(c6);
            coins.add(c7);
            coins.add(c8);

            //Add new coins to providers and types ds
            providers.get(0).getCoins().add(c0);
            types.get(0).getCoins().add(c0);
            providers.get(1).getCoins().add(c1);
            types.get(1).getCoins().add(c1);
            providers.get(1).getCoins().add(c2);
            types.get(2).getCoins().add(c2);
            providers.get(3).getCoins().add(c3);
            types.get(3).getCoins().add(c3);
            providers.get(4).getCoins().add(c4);
            types.get(4).getCoins().add(c4);
            providers.get(0).getCoins().add(c5);
            types.get(3).getCoins().add(c5);
            providers.get(5).getCoins().add(c6);
            types.get(5).getCoins().add(c6);
            providers.get(6).getCoins().add(c7);
            types.get(6).getCoins().add(c7);
            providers.get(7).getCoins().add(c8);
            types.get(7).getCoins().add(c8);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
