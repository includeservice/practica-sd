package com.example.demo.Controllers;

import com.example.demo.Classes.Coin;
import com.example.demo.Classes.Provider;
import com.example.demo.Classes.Type;
import com.example.demo.Repositories.CoinRepository;
import com.example.demo.Repositories.ProviderRepository;
import com.example.demo.Repositories.TypeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Controller for coins
 */
@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "Duplicates"})
@Controller
public class CoinsController {

    //Repositories:
    @Autowired
    CoinRepository coinRepository;
    @Autowired
    ProviderRepository providerRepository;
    @Autowired
    TypeRepository typeRepository;

    //Coins shown in page
    private Set<Coin> shownCoins = new HashSet<>();

    /**
     * Show the page with all the coins added to the database
     */
    @RequestMapping("/coins")
    public String show(ModelMap model) {
        DatabaseController.selection = DatabaseController.COINS;
        shownCoins.clear();
        shownCoins.addAll(coinRepository.findAll());

        model.addAttribute(DatabaseController.COINS, shownCoins);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.COINS);
        return "explorer_database";
    }

    /**
     * Show a form in order to insert new coins into the database
     */
    @RequestMapping("/new_coin")
    public String newCoin(ModelMap model){
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.COINS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.CREATE);
        model.addAttribute("providers1", providerRepository.findAll());
        model.addAttribute("types1", typeRepository.findAll());

        return "input_form_database";
    }

    /**
     * Show a form in order to edit a new coins into the database
     */
    @RequestMapping("/edit_coin={id}")
    public String editCoin(@PathVariable(value = "id")long id, ModelMap model){
        model.addAttribute("Coin", coinRepository.findById(id));
        model.addAttribute("providers1", providerRepository.findAll());
        model.addAttribute("types1", typeRepository.findAll());
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.COINS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.EDIT);
        return "input_form_database";
    }

    /**
     * Save a Coin into database from a html form
     */
    @Transactional
    @RequestMapping("/update_coin={id}")
    public ModelAndView updateCoin(
            @PathVariable(value = "id") long id,
            @RequestParam(value="year")int year,
            @RequestParam(value="coinageCity")String coinageCity,
            @RequestParam(value="acquisitionDate")Date acquisitionDate,
            @RequestParam(value="conservation")String conservation,
            @RequestParam(value="type")long type,
            @RequestParam(value="provider")String provider,
            ModelMap model){

        Type type_ = typeRepository.findById(type);
        Provider provider_ = null;
        try {
            provider_= providerRepository.findByCif(provider).get(0);
        }catch (Exception ignore){}

        if(provider_==null) {
            model.addAttribute("AlertProvider", Boolean.TRUE);
            model.addAttribute("Name", provider);
            return new ModelAndView("forward:/edit_coin="+id, model);
        }else if(type_ == null){
            model.addAttribute("AlertType", Boolean.TRUE);
            model.addAttribute("Name", String.valueOf(type));
            return new ModelAndView("forward:/edit_coin="+id, model);
        }

        coinRepository.updateCoin(id, year, coinageCity, acquisitionDate, conservation, type_, provider_);
        return new ModelAndView("redirect:/coins", model);
    }

    @RequestMapping("/save_coin")
    public ModelAndView saveCoin(
            @RequestParam(value="year")int year,
            @RequestParam(value="coinageCity")String coinageCity,
            @RequestParam(value="acquisitionDate")Date acquisitionDate,
            @RequestParam(value="conservation")String conservation,
            @RequestParam(value="type")long type,
            @RequestParam(value="provider")String provider,
            ModelMap model){

        Type type_ = typeRepository.findById(type);
        Provider provider_ = null;
        try {
            provider_= providerRepository.findByCif(provider).get(0);
        }catch (Exception ignore){}

        if(provider_==null) {
            model.addAttribute("AlertProvider", Boolean.TRUE);
            model.addAttribute("Name", provider);
            return new ModelAndView("forward:/new_coin", model);
        }else if(type_ == null){
            model.addAttribute("AlertType", Boolean.TRUE);
            model.addAttribute("Name", String.valueOf(type));
            return new ModelAndView("forward:/new_coin", model);
    }
        coinRepository.save(new Coin(type_,year,coinageCity,acquisitionDate,conservation,provider_));
        return new ModelAndView("redirect:/coins", model);
    }

    @RequestMapping("/delete_coin={id}")
    public RedirectView deleteCoin(@PathVariable(value = "id")long id){
        if(coinRepository.existsById(id))
            coinRepository.deleteById(id);
        return new RedirectView("/coins");
    }

    /**
     * Queries the database depending on the search variable
     */
    @RequestMapping("/search_coin")
    public String searchCoin(@RequestParam(value = "search") String search, Model model){
        shownCoins.clear();
        try {
            shownCoins.addAll(coinRepository.findByAcquisitionDate((Date) new SimpleDateFormat().parse(search)));
        } catch (Exception ignored) {}
        try {
            shownCoins.addAll(coinRepository.findByYear(Integer.parseInt(search)));
        } catch (Exception ignored) {}
        try {
            shownCoins.addAll(coinRepository.findByTypeValue(Integer.parseInt(search)));
        } catch (Exception ignored) {}
        try {
            coinRepository.findById(Long.valueOf(search)).ifPresent(shownCoins::add);
        } catch (Exception ignored){}

        shownCoins.addAll(coinRepository.findByCoinageCity(search));
        shownCoins.addAll(coinRepository.findByConservation(search));
        shownCoins.addAll(coinRepository.findByProviderName(search));

        model.addAttribute(DatabaseController.COINS, shownCoins);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.COINS);

        return "explorer_database";
    }

    /**
     *  Sort the shown list of coins depending on the sortCoin variable
     */
    @RequestMapping("/sort_coin={sortCoin}")
    public String sortCoin(@PathVariable(value = "sortCoin") Coin.SortCoin sortCoin, Model model){
        List<Coin> allCoins = new ArrayList<>();

        if(shownCoins.isEmpty())
            shownCoins.addAll(coinRepository.findAll());

        switch (sortCoin){
            case YEAR:
                allCoins.addAll(coinRepository.OrderByYear());
                break;
            case TYPE:
                allCoins.addAll(coinRepository.OrderByType());
                break;
            case ACQUISITION_DATE:
                allCoins.addAll(coinRepository.OrderByAcquisitionDate());
                break;
            case COINAGE_CITY:
                allCoins.addAll(coinRepository.OrderByCoinageCity());
                break;
            case CONSERVATION:
                allCoins.addAll(coinRepository.OrderByConservation());
                break;
            case PROVIDER:
                allCoins.addAll(coinRepository.OrderByProvider());
                break;
            default:
                allCoins.addAll(shownCoins);
        }

        allCoins.retainAll(shownCoins);

        model.addAttribute(DatabaseController.COINS, allCoins);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.COINS);

        return "explorer_database";
    }
}