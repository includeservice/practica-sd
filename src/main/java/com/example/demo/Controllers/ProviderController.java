package com.example.demo.Controllers;

import com.example.demo.Classes.Provider;
import com.example.demo.Repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Provides Controller
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Controller
public class ProviderController {

    //REPOSIRORY:
    @Autowired
    ProviderRepository providerRepository;
    private Set<Provider> shownProviders = new HashSet<>();

    /**
     * show the providers contained in the database
     */
    @RequestMapping("/providers")
    public String show(Model model) {
        DatabaseController.selection = DatabaseController.PROVIDERS;
        shownProviders.clear();
        shownProviders.addAll(providerRepository.findAll());
        model.addAttribute(DatabaseController.PROVIDERS, shownProviders);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.PROVIDERS);
        return "explorer_database";
    }

    /**
     * Show a form to enter a new provider in the database
     */
    @RequestMapping("/new_provider")
    public String newProvider(Model model){
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.PROVIDERS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.CREATE);
        return "input_form_database";
    }

    /**
     * Show a form to edit a provider in the database
     */
    @RequestMapping("/edit_provider={CIF}")
    public String editProvider(@PathVariable(value = "CIF")String cif, Model model){
        providerRepository.findById(cif).ifPresent(provider1 -> model.addAttribute("provider", provider1));
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.PROVIDERS);
        model.addAttribute(DatabaseController.MODE, DatabaseController.EDIT);
        return "input_form_database";
    }

    @RequestMapping("/delete_provider={CIF}")
    public RedirectView deleteCoin(@PathVariable(value = "CIF")String cif){
        if(providerRepository.existsById(cif))
            providerRepository.deleteById(cif);
        return new RedirectView("/providers");
    }

    @Transactional
    @RequestMapping("/update_provider={CIF}")
    public RedirectView updateProvider(@PathVariable(value = "CIF")String cif, Provider provider){
        providerRepository.updateProvider(cif, provider.getName(), provider.getAddress(),
                provider.getEmail(), provider.getPhone());
        return new RedirectView("/providers");
    }

    /**
     * Insert provider in the database
     */
    @RequestMapping("/save_provider")
    public RedirectView saveProvider(Provider provider){
        providerRepository.save(provider);
        return new RedirectView("/providers");
    }

    /**
     * Select providers from the database depending on the search variable
     */
    @RequestMapping("/search_provider")
    public String searchCoin(@RequestParam(value = "search") String search, Model model){
        shownProviders.clear();
        shownProviders.addAll(providerRepository.findByName(search));
        shownProviders.addAll(providerRepository.findByCif(search));
        shownProviders.addAll(providerRepository.findByAddress(search));
        shownProviders.addAll(providerRepository.findByEmail(search));
        shownProviders.addAll(providerRepository.findByPhone(search));

        model.addAttribute(DatabaseController.PROVIDERS, shownProviders);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.PROVIDERS);

        return "explorer_database";
    }

    /**
     * Sort the shown Providers depending on the sort variable
     */
    @RequestMapping("/sort_provider={sort}")
    public String sortProviders(@PathVariable(value = "sort") Provider.SortProvider sort, Model model){
        List<Provider> allProvider = new ArrayList<>();
        if(shownProviders.isEmpty())
            shownProviders.addAll(providerRepository.findAll());

        switch (sort){
            case CIF:
                allProvider.addAll(providerRepository.OrderByCif());
                break;
            case ADDRESS:
                allProvider.addAll(providerRepository.OrderByAddress());
                break;
            case EMAIL:
                allProvider.addAll(providerRepository.OrderByEmail());
                break;
            case NAME:
                allProvider.addAll(providerRepository.OrderByName());
                break;
            case PHONE:
                allProvider.addAll(providerRepository.OrderByPhone());
                break;
            default:
                allProvider.addAll(shownProviders);
        }

        allProvider.retainAll(shownProviders);

        model.addAttribute(DatabaseController.PROVIDERS, allProvider);
        model.addAttribute(DatabaseController.SELECTION, DatabaseController.PROVIDERS);

        return "explorer_database";
    }
}